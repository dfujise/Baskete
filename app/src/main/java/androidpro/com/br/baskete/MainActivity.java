package androidpro.com.br.baskete;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int pontuacaoTimeA = 0;
    private int pontuacaoTimeB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botao3PontosTimeA = (Button) findViewById(R.id.tresPontosA);
        botao3PontosTimeA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add3PontosTimeA();
            }
        });

        Button botao2PontosTimeA = (Button) findViewById(R.id.doisPontosA);
        botao2PontosTimeA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add2PontosTimeA();
            }
        });

        Button botaoTiroLivreTimeA = (Button) findViewById(R.id.tiroLivreA);
        botaoTiroLivreTimeA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add1PontosTimeA();
            }
        });

        Button botao3PontosTimeB = (Button) findViewById(R.id.tresPontosB);
        botao3PontosTimeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add3PontosTimeB();
            }
        });

        Button botao2PontosTimeB = (Button) findViewById(R.id.doisPontosB);
        botao2PontosTimeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add2PontosTimeB();
            }
        });

        Button botaoTiroLivreTimeB = (Button) findViewById(R.id.tiroLivreB);
        botaoTiroLivreTimeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add1PontosTimeB();
            }
        });

        Button botaoreiniciarPartida = (Button) findViewById(R.id.reiniciarPartida);
        botaoreiniciarPartida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reiniciarPartida();
            }
        });
    }


    public void mostrarPlacarTimeA (int pontuacao){

        TextView placarTimeA = (TextView) findViewById(R.id.placarTimeA);
        placarTimeA.setText(String.valueOf(pontuacao));
    }

    public void add3PontosTimeA() {
        pontuacaoTimeA = pontuacaoTimeA + 3;
        mostrarPlacarTimeA(pontuacaoTimeA);

    }

    public void add2PontosTimeA() {
        pontuacaoTimeA = pontuacaoTimeA + 2;
        mostrarPlacarTimeA(pontuacaoTimeA);
    }

    public void add1PontosTimeA() {
        pontuacaoTimeA = pontuacaoTimeA + 1;
        mostrarPlacarTimeA(pontuacaoTimeA);
    }

    public void mostrarPlacarTimeB (int pontuacao){

        TextView placarTimeB = (TextView) findViewById(R.id.placarTimeB);
        placarTimeB.setText(String.valueOf(pontuacao));
    }

    public void add3PontosTimeB() {
        pontuacaoTimeB = pontuacaoTimeB + 3;
        mostrarPlacarTimeB(pontuacaoTimeB);

    }

    public void add2PontosTimeB() {
        pontuacaoTimeB = pontuacaoTimeB + 2;
        mostrarPlacarTimeB(pontuacaoTimeB);
    }

    public void add1PontosTimeB() {
        pontuacaoTimeB = pontuacaoTimeB + 1;
        mostrarPlacarTimeB(pontuacaoTimeB);
    }
    public void reiniciarPartida(){
        pontuacaoTimeA = 0;
        pontuacaoTimeB = 0;
        mostrarPlacarTimeA(pontuacaoTimeA);
        mostrarPlacarTimeB(pontuacaoTimeB);

    }
}
